<?php

namespace common\models;

class DenyEmails extends \yii\db\ActiveRecord
{
	public function rules()
	{
		return [
			[['email', 'timestamp'], 'required'],
			[['timestamp'], 'integer'],
			[['email'], 'email']
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'email' => 'Email',
			'timestamp' => 'Timestamp',
		];
	}
}
