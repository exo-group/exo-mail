<?php

namespace common\models;

class Emails extends \yii\db\ActiveRecord
{
	public function rules()
	{
		return [
			[['email', 'template', 'timestamp'], 'required'],
			[['email'], 'email'],
			[['send', 'timestamp'], 'integer'],
			[['email', 'template'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'email' => 'Email',
			'template' => 'Template',
			'send' => 'Send',
			'timestamp' => 'Timestamp',
		];
	}
}
