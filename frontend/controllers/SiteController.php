<?php

namespace frontend\controllers;

use yii\helpers\Json;
use yii\web\Controller;
use common\models\DenyEmails;
use common\models\Emails;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionError()
    {
        return $this->render('index');
    }

    public function actionUnsubscribe()
    {
        if (!empty($_GET['code'])) {
            $email = base64_decode($_GET['code']);

            $deny_email = DenyEmails::find()->where('email = :email', [':email' => $email])->one();
            if (!$deny_email) {
                $deny_email = new DenyEmails;
            }

            $deny_email->email = $email;
            $deny_email->timestamp = time();
            $deny_email->save();

            return $this->render('unsubscribe');
        }
    }

    public function actionSendsdata()
    {
        $result = [
            'mails' => Emails::find()->count(),
            'sended' => Emails::find()->where(['send' => '1'])->count(),
            'unsubscribed' => DenyEmails::find()->count(),
        ];

        echo Json::encode($result);
    }
}
