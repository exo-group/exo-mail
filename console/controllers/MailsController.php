<?php

namespace console\controllers;

use yii;
use common\models\Emails;

class MailsController extends \yii\console\Controller
{
    public function actionSend()
    {
        $mails = Emails::find()
            ->where(['send' => 0])
            ->orderBy('id')
            ->limit(30)
            ->all();

        $last_mail = Emails::find()
            ->where(['send' => 1])
            ->orderBy(['timestamp' => SORT_DESC])
            ->one();

        if ($last_mail && time() - $last_mail->timestamp < 1) {
            sleep(1);
        }

        $mandrill = Yii::$app->mandrill;

        echo "\n";
        foreach ($mails as $mail) {
            echo "Send to " . $mail->email . " ... ";
            $message = [
                'html' => Yii::$app->view->render('@backend/mails/' . $mail->template, ['unlinkcode' => base64_encode($mail->email)]),
                'subject' => Yii::$app->view->title,
                'from_email' => Yii::$app->params['spamEmail'],
                'from_name' => Yii::$app->params['spamName'],
                'to' => [
                    [
                        'email' => $mail->email,
                        'type' => 'to'
                    ]
                ],
                'track_opens' => true,
                'track_clicks' => true,
                /*'attachments' => [
                    [
                        'type' => 'text/plain',
                        'name' => 'myfile.txt',
                        'content' => 'ZXhhbXBsZSBmaWxl'
                    ]
                ],*/
                /*'images' => [
                    [
                        'type' => 'image/png',
                        'name' => 'IMAGECID',
                        'content' => 'ZXhhbXBsZSBmaWxl'
                    ]
                ]*/
            ];

            $async = false;
            $ip_pool = null;
            $send_at = null;

            try {
                $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);

                $mail->send = 1;
                $mail->timestamp = time();
                $mail->save();
                echo "Done\n";
            } catch(Exception $e) {}

            sleep(1);
        }
    }
}