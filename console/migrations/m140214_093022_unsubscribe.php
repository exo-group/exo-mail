<?php

use yii\db\Schema;

class m140214_093022_unsubscribe extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('tbl_deny_emails', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'timestamp' => Schema::TYPE_INTEGER.' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('tbl_deny_emails');
    }
}
