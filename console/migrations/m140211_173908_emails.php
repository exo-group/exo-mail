<?php

use yii\db\Schema;

class m140211_173908_emails extends \yii\db\Migration
{
	public function up()
	{
        $this->createTable('tbl_emails', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'template' => Schema::TYPE_STRING . ' NOT NULL',
            'send' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'timestamp' => Schema::TYPE_INTEGER.' NOT NULL',
        ]);
	}

	public function down()
	{
        $this->dropTable('tbl_emails');
	}
}
