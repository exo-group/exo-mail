<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>50 USD welcome bonus</title>
        <style>
            body {
                background: #f2f2f2;
                font-family: Verdana;
                color: #606060;
                font-size: 14px;
            }
            #container {
                background: #fff;
                margin:0 10% 0 10%;
                width:80%;
            }
            #main {
                padding: 30px;
            }
            #footer{
                padding: 20px;
                font-size: 10px;
                text-align: center;
            }
            a{
                color: #008aaf;
                text-decoration: underline;
                font-weight: bold;
            }
            a:hover{
                text-decoration: none;
            }
            hr {
                border: 1px solid #cecece;
                margin: 30px 0 30px 0;
            }
        </style>
    </head>
    <body>
        <div id="header"></div>
        <div id="container">
            <div id="main">
                <a href="http://exo-group.com" target="_blank"><img src="http://exo-mail.com/images/logo.png"></a>
                <h1>Dear Sir</h1>
                <p><strong>Our 50 USD welcome bonus is made for CIS countries' citizens only.</strong> We plan to present a bonus for Asian traders later in March.<br />
                <p>We will keep you updated and thank you for your interest in our company.</p>
                <p>Best regards,<br />EXO Group</p>
                <hr>
                <a href="http://exo-group.com" target="_blank">www.exo-group.com</a>, <a href="mailto:mail@exo-group.com">mail@exo-group.com</a></p>
            </div>
        </div>
    </body>
</html>