<?php $this->title = "Легкий способ сохранить и приумножить свои накопления"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Легкий способ сохранить и приумножить свои накопления</title>
		<style>
			body {
				background: #f2f2f2;
				font-family: Verdana;
				color: #606060;
				font-size: 14px;
			}
			table { 
				width: 100%;
				border: 1px solid #ececece;
				border-collapse: collapse;
				text-align: center;
				font-size: 12px;
			}
			td { 
				padding: 5px;
				border: 1px solid #cecece;
			}
			.colored_th {
				background: #f0f0f0;
				font-weight: bold;
			}
			.colored {
				background: #f9f9f9;
			}
			#container {
				background: #fff;
				margin:0 10% 0 10%;
				width:80%;
			}
			#main {
				padding: 30px;
			}
			#footer{
				padding: 20px;
				font-size: 10px;
				text-align: center;
			}
			a{
				color: #008aaf;
				text-decoration: underline;
				font-weight: bold;
			}
			a:hover{
				text-decoration: none;
			}
			hr {
				border: 1px solid #cecece;
				margin: 30px 0 30px 0;
			}
		</style>
	</head>
	<body>
		<div id="header"></div>
		<div id="container">
			<div id="main">
				<a href="http://exo-group.ru" target="_blank"><img src="http://exo-mail.com/images/logo.png"></a>
				<h1>Добрый день!</h1>
				<p>Компания EXO Group предлагает, Вам, с помощью рынка FOREX сохранить и приумножить Ваши денежные средства.</p>
				<p>Новозеландская компания <a href="http://exo-group.ru" target="_blank">EXO Group</a> недавно работающая но, активно развивающаяся на рынке России, предлагает Вам воспользоваться брокерскими услугами на валютном  рынке Forex. Forex — это глобальный международный рынок, товаром на котором выступают валюты. Его отличительной особенностью является то, что цена валюты формируется на основе соглашения между участниками рынка и зависит только от спроса и предложения на ту или иную валюту. </p>
				<h3>Что мы предлагаем?</h3>
				<ul>
					<li>Широкий спектр услуг и менеджмента Европейского уровня.</li>
					<li>Удобный ввод и вывод денежных средств.</li>
					<li>Профессиональную службу поддержки.</li>
				</ul>
				<p>Мы предоставим вам все необходимое <a href="http://exo-group.ru/trading/platform" target="_blank">программное обеспечение</a> и научим вас им пользоваться. Наши сотрудники грамотно ответят на ваши вопросы, и дадут необходимые консультации.</p>
				<h3>Торговля на Forex</h3>
				<p>Начать свою торговлю на рынке Forex можно имея 100$. Для того чтобы разобраться со всеми нюансами торговли, компания предлагает <a href="http://exo-group.ru/education" target="_blank">бесплатное обучение</a> и <a href="http://exo-group.ru/trading/newcomer" target="_blank">счет тренажер</a> для оттачивания мастерства торговли на рынке Forex.</p>
				<p>Наши консультанты научат Вас торговать, и будут поддерживать советами и аналитикой во время торговли.</p>
				<h3>О компании</h3>
				<ul>
					<li>Компания EXO Group является профессиональным участником рынка Forex и предоставляет брокерские услуги более чем в 30 странах мира. Компания  зарегистрирована в Financial Service Provider Register и имеет лицензию под номером <a href="http://exo-group.ru/exogroup/about" target="_blank">FSP246525</a>. </li>
					<li>Компания проходит обязательный ежегодный аудит, и предоставляет финансовый отчет о своей деятельности, в рамках программы выполнения и соблюдения требований финансовой дисциплины, предусмотренной законодательством страны юрисдикции. </li>
					<li>EXO Group является участником международной <a href="http://exo-group.ru/exogroup/license" target="_blank">системы стандартизации</a> работы с клиентами: AML (Anti-Money Laundering System) и KYC (Know Your Customer). </li>
					<li>Профессиональная международная лицензия Forex и стандартизация работы согласно мировым требованиям по передаче и хранению информации – это залог стабильной, безопасной работы компании и гарантированных выплат Клиентам.</li>
				</ul>
				<h3>Подведем итог</h3>
				<p>Итак, мы рассказали, Вам, что такое Forex и о том, как просто на нем зарабатывать. Теперь вы можете обратиться в <a href="http://exo-group.ru/exogroup/contacts" target="_blank">службу поддержки</a> компании или перейти на наш сайт и <a href="http://exo-group.ru/trading/conditions?selected_col=1" target="_blank">открыть свой счет</a> на международном рынке Forex.</p>
				<p>С уважением,<br />Команда EXO Group</p>
				<hr>
				<h3>Контактная информация</h3>
				<p>20-22 Munroe St, Napier, 4110, New Zealand<br />Телефон русскоязычной службы поддержки: 8 800 505 20 45<br /><a href="http://exo-group.ru" target="_blank">www.exo-group.ru</a>, <a href="http://mailto:mail@exo-group.com">mail@exo-group.com</a></p>
			</div>
		</div>
		<div id="footer">Чтобы отписаться от этой рассылки, перейдите по <a href="http://exo-mail.com/unsubscribe?code=<?=$unlinkcode?>" target="_blank">ссылке</a></div>
	</body>
</html>