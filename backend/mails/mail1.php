<?php $this->title = "Предложение для успешного трейдинга"; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Предложение для успешного трейдинга</title>
        <style>
            body {
                background: #f2f2f2;
                font-family: Verdana;
                color: #606060;
                font-size: 14px;
            }
            table {
                width: 100%;
                border: 1px solid #ececece;
                border-collapse: collapse;
                text-align: center;
                font-size: 12px;
            }
            td {
                padding: 5px;
                border: 1px solid #cecece;
            }
            .colored_th {
                background: #f0f0f0;
                font-weight: bold;
            }
            .colored {
                background: #f9f9f9;
            }
            #container {
                background: #fff;
                margin:0 10% 0 10%;
                width:80%;
            }
            #main {
                padding: 30px;
            }
            #footer{
                padding: 20px;
                font-size: 10px;
                text-align: center;
            }
            a{
                color: #008aaf;
                text-decoration: underline;
                font-weight: bold;
            }
            a:hover{
                text-decoration: none;
            }
            hr {
                border: 1px solid #cecece;
                margin: 30px 0 30px 0;
            }
        </style>
    </head>
    <body>
        <div id="header"></div>
        <div id="container">
            <div id="main">
                <a href="http://exo-group.ru" target="_blank"><img src="http://exo-mail.com/images/logo.png"></a>
                <h1>Добрый день!</h1>
                <p>Мы хотим предложить Вам качественно новый уровень работы на рынке Forex. Наша компания гарантирует выполнение заявленных торговых условий. Мы предлагаем своим клиентам возможность реализации любых торговых стратегий: будь то автоматическая торговля, торговля по новостям или скальпинг.</p>
                <h3>О компании</h3>
                <p>Компания <a href="http://exo-group.ru" target="_blank">EXO Group</a> — это команда профессионалов, обеспечивающих условия стабильной и безопасной работы на рынке Forex. Мы прекрасно понимаем, какое значение в нашей работе имеет доверие клиентов. Именно поэтому мы строим свой бизнес таким образом, чтобы создать все условия для успешной торговли клиента.</p>
                <h3>Регулирование</h3>
                <p>EXO Group зарегистрирована в Financial Service Providers Register в Новой Зеландии под номером <a href="http://exo-group.ru/exogroup/about" target="_blank">FSP328346</a>. Компания проходит обязательный ежегодный аудит, и предоставляет финансовый отчет о своей деятельности, в рамках программы выполнения требований финансовой дисциплины, предусмотренной законодательством страны юрисдикции.</p>
                <h3>Преимущества</h3>
                <ul>
                    <li>работа по системе NDD</li>
                    <li>отсутствие комиссий</li>
                    <li>спреды от 0.7 пункта</li>
                    <li>депозит от 100 USD</li>
                    <li>начисляем 7% годовых в USD на неторговую часть депозита</li>
                    <li>отсутствие реквот</li>
                    <li>обработка ордеров по технологии STP</li>
                    <li>поставщики ликвидности: Currenex, Integral</li>
                </ul>
                <h3>Выдержка из <a href="http://exo-group.ru/trading/conditions" target="_blank">торговых условий</a> компании, счет NDD Pro</h3>
                <table border="1" bordercolor="#ececece">
                    <tr class="colored_th">
                        <td>Инструмент</td>
                        <td>Минимальный спред</td>
                        <td>Средний спред</td>
                        <td>Stop & Limit Levels</td>
                        <td>SWAP Long</td>
                        <td>SWAP Short</td>
                    </tr>
                    <tr>
                        <td>EURUSD</td>
                        <td>0,7</td>
                        <td>1,1</td>
                        <td>2</td>
                        <td>-0,142</td>
                        <td>-0,088</td>
                    </tr>
                    <tr class="colored">
                        <td>GBPUSD</td>
                        <td>1,1</td>
                        <td>1,5</td>
                        <td>3</td>
                        <td>-0,053</td>
                        <td>-0,257</td>
                    </tr>
                    <tr>
                        <td>USDCHF</td>
                        <td>1,2</td>
                        <td>2,1</td>
                        <td>3</td>
                        <td>-0,037</td>
                        <td>-0,137</td>
                    </tr>
                    <tr class="colored">
                        <td>EURGBP</td>
                        <td>1,1</td>
                        <td>1,7</td>
                        <td>3</td>
                        <td>-0,154</td>
                        <td>0,012</td>
                    </tr>
                    <tr>
                        <td>GBPJPY</td>
                        <td>2,0</td>
                        <td>3,3</td>
                        <td>4</td>
                        <td>0</td>
                        <td>-0,259</td>
                    </tr>
                </table>
                <p>Мы ценим Ваш опыт и предлагаем Вам уже сегодня начать работу с <a href="http://exo-group.ru/user/signup" target="_blank">нашей компанией</a>.</p>
                <p>С уважением,<br />Команда EXO Group</p>
                <hr>
                <h3>Контактная информация</h3>
                <p>20-22 Munroe St, Napier, 4110, New Zealand<br />Телефон русскоязычной службы поддержки: 8 800 505 20 45<br /><a href="http://exo-group.ru" target="_blank">www.exo-group.ru</a>, <a href="http://mailto:mail@exo-group.com">mail@exo-group.com</a></p>
            </div>
        </div>
        <div id="footer">Чтобы отписаться от этой рассылки, перейдите по <a href="http://exo-mail.com/unsubscribe?code=<?=$unlinkcode?>" target="_blank">ссылке</a></div>
    </body>
</html>