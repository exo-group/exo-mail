<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class FormModel extends Model
{
    public $mail;
    public $file;

    public function rules()
    {
        return [
            [['mail', 'file'], 'required'],
            ['file', 'file', 'types' => ['csv']],
        ];
    }
}