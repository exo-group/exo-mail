<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var yii\web\View $this
 */
$this->title = 'My Yii Application';
?>

<?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
	<?= $form->field($model, 'mail')->radioList($mails) ?>
	<?= $form->field($model, 'file')->fileInput() ?>
	<div class="form-group">
		<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
	</div>
<?php ActiveForm::end(); ?>