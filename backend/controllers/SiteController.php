<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use backend\models\FormModel;
use common\models\Emails;
use common\models\LoginForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\web\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new FormModel;

        if ($model->load($_POST)) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                Emails::deleteAll();
                $rows = explode("\n", file_get_contents($model->file->tempName));
                foreach ($rows as $row) {
                    $row = str_getcsv($row, ";");
                    if (!empty($row[0])) {
                        $email = new Emails;
                        $email->email = $row[0];
                        $email->template = $model->mail;
                        $email->send = 0;
                        $email->timestamp = 0;
                        $email->save();
                    }
                }
            }
        }

        $mails = [];
        foreach (scandir(Yii::getAlias('@backend/mails')) as $file) {
            if (is_dir($file) || in_array($file, ['.', '..'])) {
                continue;
            }
            $mails[$file] = $file;
        }

        return $this->render('index', [
            'model' => $model,
            'mails' => $mails,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load($_POST) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
