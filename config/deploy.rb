set :application, 'exo-mail'
set :repo_url, 'git@bitbucket.org:exo-group/exo-mail.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :deploy_to, '/var/www/exo-mail.com/'
set :scm, :git

# set :format, :pretty
set :log_level, :debug
# set :pty, true

#set :linked_files, %w{www/protected/config/main_local.php www/protected/config/console_local.php}
set :linked_dirs, %w{backend/runtime backend/web/assets frontend/runtime frontend/web/assets console/runtime}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      execute "cd #{release_path} && php composer.phar update"
      execute "cd #{release_path} && ./init --env=Production"
      execute "rm -rf #{shared_path}/backend/web/assets/* #{shared_path}/frontend/web/assets/*"
    end
  end

  # run migrations
  desc "Run migrations"
  task :migrate do
    on roles(:web) do
      execute "#{release_path}/yii migrate --interactive=0"
    end
  end

  after :finishing, 'deploy:migrate'
  after :finishing, 'deploy:cleanup'

end
